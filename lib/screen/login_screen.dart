// ignore_for_file: use_build_context_synchronously

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:dataon_app/screen/dashboard_screen.dart';
import 'package:dataon_app/screen/register_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController txtusername = TextEditingController();
  TextEditingController txtpassword = TextEditingController();

  String username = '';
  String password = '';

  bool _secureText = true;
  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  void getUser() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    username = (preferences.getString('username') ?? '');
    password = (preferences.getString('password') ?? '');
  }

  void login() async {
    if (txtusername.text == username && txtpassword.text == password) {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      await preferences.setBool('isLoggin', true);
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => const DashboardScreen()),
          (route) => false);
      AnimatedSnackBar.material('Login berhasil',
              type: AnimatedSnackBarType.success,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    } else {
      AnimatedSnackBar.material('Username atau password salah',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  @override
  void initState() {
    getUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: ListView(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: SizedBox(
                      height: 300,
                      child: Image.asset('assets/image/dataon.png'),
                    ),
                  ),
                  const Text(
                    'Login',
                    style: TextStyle(
                      fontSize: 30,
                      fontFamily: "PoppinsReguler",
                      fontWeight: FontWeight.bold,
                      // color: blueColor,
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  const Text(
                    'Username',
                    style: TextStyle(
                      fontSize: 16,
                      fontFamily: "PoppinsReguler",
                      fontWeight: FontWeight.bold,
                      // color: blueColor,
                    ),
                  ),
                  Container(
                    height: 45,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey, width: 1.0),
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: TextFormField(
                        controller: txtusername,
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    'Password',
                    style: TextStyle(
                      fontSize: 16,
                      fontFamily: "PoppinsReguler",
                      fontWeight: FontWeight.bold,
                      // color: blueColor,
                    ),
                  ),
                  Container(
                    height: 45,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey, width: 1.0),
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: TextFormField(
                        controller: txtpassword,
                        obscureText: _secureText,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          suffixIcon: IconButton(
                            onPressed: showHide,
                            icon: Icon(_secureText
                                ? Icons.visibility_outlined
                                : Icons.visibility),
                            color: const Color(0xff4B556B),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const RegisterScreen()));
                          },
                          child: const Text('Register')),
                    ],
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  GestureDetector(
                    onTap: () {
                      if (txtusername.text == '') {
                        AnimatedSnackBar.material('Username tidak boleh kosong',
                                type: AnimatedSnackBarType.warning,
                                mobileSnackBarPosition:
                                    MobileSnackBarPosition.top,
                                desktopSnackBarPosition:
                                    DesktopSnackBarPosition.topCenter)
                            .show(context);
                      } else if (txtpassword.text == '') {
                        AnimatedSnackBar.material('Password tidak boleh kosong',
                                type: AnimatedSnackBarType.warning,
                                mobileSnackBarPosition:
                                    MobileSnackBarPosition.top,
                                desktopSnackBarPosition:
                                    DesktopSnackBarPosition.topCenter)
                            .show(context);
                      } else {
                        login();
                      }
                    },
                    child: Container(
                      height: 50,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: Colors.red),
                      child: const Center(
                        child: Text(
                          'Login',
                          style: TextStyle(
                              fontSize: 16,
                              fontFamily: "PoppinsReguler",
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
