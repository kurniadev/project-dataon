import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:dataon_app/screen/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({super.key});

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  TextEditingController txtname = TextEditingController();
  TextEditingController txtusername = TextEditingController();
  TextEditingController txtpassword = TextEditingController();

  void register() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('name', txtname.text);
    await preferences.setString('username', txtusername.text);
    await preferences.setString('password', txtpassword.text);
    setState(() {
      AnimatedSnackBar.material('Registrasi Berhasil',
              type: AnimatedSnackBarType.success,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => const LoginScreen()),
          (route) => false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Register',
                style: TextStyle(
                  fontSize: 30,
                  fontFamily: "PoppinsReguler",
                  fontWeight: FontWeight.bold,
                  // color: blueColor,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                'Name',
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: "PoppinsReguler",
                  fontWeight: FontWeight.bold,
                  // color: blueColor,
                ),
              ),
              Container(
                height: 45,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: 1.0),
                    borderRadius: BorderRadius.circular(10)),
                child: Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: TextFormField(
                    controller: txtname,
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                'Username',
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: "PoppinsReguler",
                  fontWeight: FontWeight.bold,
                  // color: blueColor,
                ),
              ),
              Container(
                height: 45,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: 1.0),
                    borderRadius: BorderRadius.circular(10)),
                child: Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: TextFormField(
                    controller: txtusername,
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                'Password',
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: "PoppinsReguler",
                  fontWeight: FontWeight.bold,
                  // color: blueColor,
                ),
              ),
              Container(
                height: 45,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: 1.0),
                    borderRadius: BorderRadius.circular(10)),
                child: Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: TextFormField(
                    controller: txtpassword,
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              GestureDetector(
                onTap: () {
                  if (txtusername.text == '') {
                    AnimatedSnackBar.material('Username tidak boleh kosong',
                            type: AnimatedSnackBarType.warning,
                            mobileSnackBarPosition: MobileSnackBarPosition.top,
                            desktopSnackBarPosition:
                                DesktopSnackBarPosition.topCenter)
                        .show(context);
                  } else if (txtpassword.text == '') {
                    AnimatedSnackBar.material('Password tidak boleh kosong',
                            type: AnimatedSnackBarType.warning,
                            mobileSnackBarPosition: MobileSnackBarPosition.top,
                            desktopSnackBarPosition:
                                DesktopSnackBarPosition.topCenter)
                        .show(context);
                  } else {
                    register();
                  }
                },
                child: Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: Colors.red),
                  child: const Center(
                    child: Text(
                      'Register',
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: "PoppinsReguler",
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
