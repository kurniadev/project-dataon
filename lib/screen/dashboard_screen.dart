import 'dart:convert';

import 'package:dataon_app/screen/detail_screen.dart';
import 'package:dataon_app/screen/splashscreen.dart';
import 'package:dataon_app/screen/web_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({super.key});

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  String? date;
  String name = '';
  bool _showFab = true;
  List _items = [];

  String formatDate(DateTime dateTime) {
    return DateFormat(
      'EEEE, d MMMM yyyy',
    ).format(dateTime);
  }

  logout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setBool('isLoggin', false);
    // ignore: use_build_context_synchronously
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => const SplashScreen()),
        (route) => false);
  }

  void getUser() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    name = (preferences.getString('name') ?? '');
  }

  void getTime() {
    final DateTime now = DateTime.now();
    final String formattedDate = formatDate(now);
    setState(() {
      date = formattedDate;
    });
  }

// Fetch content from the json file
  Future<void> readJson() async {
    final String response =
        await rootBundle.loadString('assets/json/news.json');
    final data = await json.decode(response);
    setState(() {
      _items = data["news"];
    });
  }

  @override
  void initState() {
    getTime();
    readJson();
    getUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    const duration = Duration(milliseconds: 300);
    return Scaffold(
      floatingActionButton: AnimatedSlide(
        duration: duration,
        offset: _showFab ? Offset.zero : const Offset(0, 2),
        child: AnimatedOpacity(
          duration: duration,
          opacity: _showFab ? 1 : 0,
          child: FloatingActionButton(
            backgroundColor: Colors.blue,
            child: const Icon(Icons.add),
            onPressed: () {},
          ),
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ListView(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'DataOn',
                        style: TextStyle(
                          fontSize: 20,
                          fontFamily: "PoppinsReguler",
                          fontWeight: FontWeight.bold,
                          // color: blueColor,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          logout();
                        },
                        child: Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              color: Colors.red),
                          child: Icon(
                            Icons.logout,
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                  Text(
                    date ?? '',
                    style: const TextStyle(
                      fontSize: 12,
                      fontFamily: "PoppinsReguler",
                      fontWeight: FontWeight.w400,
                      // color: blueColor,
                    ),
                  ),
                  Text(
                    'Welcome,${name}',
                    style: TextStyle(
                      fontSize: 16,
                      fontFamily: "PoppinsReguler",
                      fontWeight: FontWeight.bold,
                      // color: blueColor,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 200,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        image: const DecorationImage(
                            image: NetworkImage(
                                'https://source.unsplash.com/random/400x300/?sports'),
                            fit: BoxFit.cover)),
                    child: Stack(children: [
                      Positioned(
                        left: 10,
                        bottom: 10,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const WebViewScreen(
                                          url: 'https://dataon.com/',
                                        )));
                          },
                          child: Container(
                            height: 40,
                            width: 100,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                color: Colors.red),
                            child: const Center(
                                child: Text(
                              'Visit Data On',
                              style: TextStyle(
                                fontSize: 12,
                                fontFamily: "PoppinsReguler",
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                                letterSpacing: 1,
                              ),
                            )),
                          ),
                        ),
                      )
                    ]),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text(
                        'Latest News',
                        style: TextStyle(
                          fontSize: 20,
                          fontFamily: "PoppinsReguler",
                          fontWeight: FontWeight.bold,
                          // color: blueColor,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {},
                        child: SizedBox(
                          height: 20,
                          child: Text(
                            'Latest News',
                            style: TextStyle(
                              fontSize: 15,
                              fontFamily: "PoppinsReguler",
                              fontWeight: FontWeight.w400,
                              color: Colors.blue[800],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  ListView.separated(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: _items.length,
                    separatorBuilder: (BuildContext context, int index) =>
                        const Divider(), // Tambahkan pemisah antara setiap item
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          // Navigasi ke halaman detail dengan mengirim data item yang dipilih
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  DetailScreen(item: _items[index]),
                            ),
                          );
                        },
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8),
                                        color: Colors.grey.withOpacity(0.5)),
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 2),
                                      child: Text(
                                        _items[index]["category"],
                                        style: const TextStyle(
                                          fontSize: 12,
                                          fontFamily: "PoppinsReguler",
                                          fontWeight: FontWeight.w400,
                                          // color: blueColor,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Text(
                                    _items[index]["title"],
                                    style: const TextStyle(
                                      fontSize: 16,
                                      fontFamily: "PoppinsReguler",
                                      fontWeight: FontWeight.bold,
                                      // color: blueColor,
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  Text(
                                    'Oleh ${_items[index]["author"]}',
                                    style: const TextStyle(
                                      fontSize: 12,
                                      fontFamily: "PoppinsReguler",
                                      fontWeight: FontWeight.w400,
                                      // color: blueColor,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Container(
                              height: 100,
                              width: 100,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12),
                                  image: DecorationImage(
                                      image:
                                          NetworkImage(_items[index]["image"]),
                                      fit: BoxFit.cover)),
                            )
                          ],
                        ),
                        // child: ListTile(
                        //   leading: Text(_items[index]["id"]),
                        //   title: Text(_items[index]["name"]),
                        //   subtitle: Text(_items[index]["description"]),
                        // ),
                      );
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
