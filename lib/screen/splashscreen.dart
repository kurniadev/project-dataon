import 'package:dataon_app/screen/dashboard_screen.dart';
import 'package:dataon_app/screen/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool? isLoggin;
  void loadUserInfo() async {
    await Future.delayed(const Duration(milliseconds: 3000));
    SharedPreferences preferences = await SharedPreferences.getInstance();
    isLoggin = (preferences.getBool('isLoggin') ?? false);
    if (isLoggin == true) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => const DashboardScreen()),
          (route) => false);
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => const LoginScreen()),
          (route) => false);
    }
  }

  @override
  void initState() {
    loadUserInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: SizedBox(
            height: 300,
            child: Image.asset('assets/image/dataon.png'),
          ),
        ),
      ),
    );
  }
}
